const EventEmitter = require('events');

class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */
  constructor(title) {
    super();

    this.title = title;

    // Посылать каждую секунду сообщение
    setInterval(() => {
      this.emit('message', `${this.title}: ping-pong`);
  }, 1000);
  }

  close() {
  	this.emit('close', '');
  	this.removeAllListeners();
  }
}

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');





vkChat.setMaxListeners(2);

class VkEmitter extends EventEmitter {
}

const vkEmitter = new VkEmitter();

(function vkChatOnEvent(vk) {

	vkChat.on('message', function() {
		console.log('Готовлюсь к ответу');
		vk.emit('ready', 'Готовлюсь к ответу...')
	});

	vkChat.on('close', function() {
		console.log('Чат вконтакте закрылся :(');
	});

})(vkEmitter);

vkEmitter.on ('ready', () => console.log('почти готов...'));




let chatOnMessage = (message) => {
  console.log(message);
};

webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);


// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
vkChat.close('message', chatOnMessage);

}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
facebookChat.close('message', chatOnMessage);
}, 15000 );
